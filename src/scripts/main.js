//jQuery.datetimepicker
jQuery.datetimepicker.setLocale('vi');

jQuery('#datetimepicker').datetimepicker({
 i18n:{
  de:{
   months:[
    'Januar','Februar','März','April',
    'Mai','Juni','Juli','August',
    'September','Oktober','November','Dezember',
   ],
   dayOfWeek:[
    "So.", "Mo", "Di", "Mi", 
    "Do", "Fr", "Sa.",
   ]
  }
 },
 timepicker:false,
 format:'d.m.Y'
});

//fixed ads
window.onscroll = function() {myFunction()};

var leftBanners = $(".left-banners");
var rightBanners = $(".right-banners");
var sticky = 80;

function myFunction() {
  if (window.pageYOffset > sticky) {
    leftBanners.addClass("sticky");
    rightBanners.addClass("sticky");
  } else {
    leftBanners.removeClass("sticky");
    rightBanners.removeClass("sticky");
  }
}
$('.box_timeList_title').click(function(){
    $(this).next().slideToggle( "slow" );//hide parent and show next
    $(this).toggleClass("active");
 });
