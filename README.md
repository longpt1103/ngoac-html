# [ngoac HTML](https://gitlab.com/longpt1103/ngoac-html.git)

A front-end was boilerplate for converting PSD/Sketch to HTML/CSS/JavaScripts It's focused on performance and best practices.



## Quick start

1. Download the latest stable release from
   [here](https://gitlab.com/longpt1103/ngoac-html.git) or clone this repo using `git clone --depth=1 https://gitlab.com/longpt1103/ngoac-html.git`
2. Run `yarn run setup` or `npm run setup` to install dependencies
3. Run `yarn run start` or `npm run start` and go to `http://localhost:8000`
4. User node version - 8.1

## License

The code is available under the [MIT license](LICENSE.md).
